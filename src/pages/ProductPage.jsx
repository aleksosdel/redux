import React from 'react';
import src1 from "../assets/img/1.jpg";
import {Card, Col, Row} from "react-bootstrap";
import classes from "../components/MyStyle.module.css";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
const ProductPage = () => {
    const product = useSelector(state => state.cards[0])
    return (
        <Row className='mt-4'>
            <Col md={6}>
                <Card>
                    <Card.Img variant="top" src={src1}/>
                </Card>
            </Col>
            <Col md={6}>
                <div className={classes.productPrice}>{product.price}&#8381;</div>
                <h1>{product.title}</h1>
                <div>{product.desc}</div>
                <Link to='/auth'>Auth</Link>
            </Col>
        </Row>
    );
};

export default ProductPage;