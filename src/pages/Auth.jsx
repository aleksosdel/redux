import React from 'react';
import {Container} from "react-bootstrap";
import NavBar from "../components/NavBar";
import ProductPage from "./ProductPage";

const Auth = () => {
    return (
        <>
            <NavBar/>
            <Container>
                <ProductPage/>
            </Container>
        </>
    );
};

export default Auth;