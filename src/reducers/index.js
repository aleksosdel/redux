import src1 from "../assets/img/1.jpg";
import src2 from "../assets/img/2.jpg";

const initialState = {
    cards: [
        {
            id: 1,
            title: 'pizza five cheese',
            price: '540',
            desc: 'Даже самый искушенный кулинарный эксперт не сможет пройти мимо этого блюда',
            img: src1,
        },
        {
            id: 2,
            title: 'pizza five cheese',
            price: '540',
            desc: 'Даже самый искушенный кулинарный эксперт не сможет пройти мимо этого блюда',
            img: src2,
        },
    ]
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state
    }
}
