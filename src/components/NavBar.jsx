import React from 'react';
import {Container, Nav, Navbar} from "react-bootstrap";
import './NavBar.css'
import {Link} from "react-router-dom";
const NavBar = () => {
    return (
        <>
            <Navbar bg='warning'>
                <Container>
                    <div className='logo'>Moovee</div>
                    <Nav>
                        <Link to='/'>Home</Link>
                        <Link className='ms-2' to='/auth'>Auth</Link>
                    </Nav>
                </Container>
            </Navbar>
        </>
    );
};

export default NavBar;